import copy
import config
import random

class Agent:
    def __init__(self):
        self.action_number = 5
        self.state_number = config.game_board_width * config.game_board_high
        self.Q = []
        for q in range(0, self.state_number):
            q_ac = []
            for a in range(self.action_number):
                q_ac.append(config.learning_first_q)
            self.Q.append(copy.copy(q_ac))

    @staticmethod
    def state_to_unique_number(x, y):
        return y * config.game_board_width + x

    def get_best_action_greedy(self, x, y):
        state = Agent.state_to_unique_number(x, y)
        best_q = self.Q[state][0]
        best_action = 0
        for a in range(self.action_number):
            if self.Q[state][a] > best_q:
                best_q = self.Q[state][a]
                best_action = a
        return best_action

    def get_random_action(self):
        return random.randint(0, 5)

    def update_qlearning(self, x, y, r, a, xp, yp):
        state = Agent.state_to_unique_number(x, y)
        statep = Agent.state_to_unique_number(xp, yp)
        new_q = r + max(self.Q[statep])
        self.Q[state][a] += new_q
